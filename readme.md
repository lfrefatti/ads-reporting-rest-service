# Advertising Report Rest API

This service provides advertising analytics data.

API documentation can be found on docs/public or {host}/v2/api-docs

## Prerequisites

  * JDK 8
  * Maven 3.5.0

## Running tests

```bash
  >> mvn verify
```

## Build
    
```bash
  >> mvn install
```

## Usage

```bash
  >> java -jar {jarFile}
```

## Authors
  * Luís Fernando Refatti
