package com.lfrefatti.adsreporting.enums;

public enum Months {
    JAN, FEV, MAR, APR, MAY, JUN, JUL, AUG, SEP, OCT, NOV, DEC;

    public int getValue() {
        return ordinal() + 1;
    }
}
