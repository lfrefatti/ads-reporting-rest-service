package com.lfrefatti.adsreporting.dto;

import com.fasterxml.jackson.annotation.*;
import com.lfrefatti.adsreporting.swagger.DocumentedAnalyticsDTO;
import com.lfrefatti.adsreporting.utils.CsvParserContract;
import com.lfrefatti.adsreporting.utils.MathFunctions;

import static com.lfrefatti.adsreporting.utils.Calculator.*;

import javax.persistence.*;
import java.math.BigDecimal;
import java.time.Month;
import java.time.format.TextStyle;
import java.util.Locale;

@Entity
@Table(name = "analytics")
public class AnalyticsDTO implements DocumentedAnalyticsDTO, CsvParserContract {

    @Id
    @GeneratedValue
    @JsonIgnore
    private Integer id;
    @JsonIgnore
    private Integer month;
    @JsonInclude(JsonInclude.Include.NON_NULL)
    private String site;
    private Long requests;
    private Long impressions;
    private Long clicks;
    private Long conversions;
    private Double revenue;
    @JsonProperty("CTR")
    private Double ctr;
    @JsonProperty("CR")
    private Double cr;
    @JsonProperty("fill_rate")
    private Double fillRate;
    private Double eCPM;

    public AnalyticsDTO(){}

    public AnalyticsDTO(String site, Long requests, Long impressions, Long clicks, Long conversions, Double revenue){
        this.site = site;
        this.requests = requests;
        this.impressions = impressions;
        this.clicks = clicks;
        this.conversions = conversions;
        this.revenue = revenue;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Integer getMonth() { return month;}

    public void setMonth(Integer month) {
        this.month = month;
    }

    @JsonProperty("month")
    @JsonInclude(JsonInclude.Include.NON_NULL)
    public String getMonthName(){
        if (getMonth() == null)
            return null;

        return Month.of(getMonth()).getDisplayName(TextStyle.FULL, Locale.getDefault());
    }

    public String getSite() {
        return site;
    }

    public void setSite(String site) {
        this.site = site;
    }

    public Long getImpressions() {
        return impressions;
    }

    public void setImpressions(Long impressions) {
        this.impressions = impressions;
    }

    public Long getClicks() {
        return clicks;
    }

    public void setClicks(Long clicks) {
        this.clicks = clicks;
    }

    public Long getConversions() {
        return conversions;
    }

    public void setConversions(Long conversions) {
        this.conversions = conversions;
    }

    public Double getCtr() {
        return ctr;
    }

    public void setCtr(Double ctr) {
        this.ctr = ctr;
    }

    public Double getCr() {
        return cr;
    }

    public void setCr(Double cr) {
        this.cr = cr;
    }

    public Double getFillRate() {
        return fillRate;
    }

    public void setFillRate(Double fillRate) {
        this.fillRate = fillRate;
    }

    public Double geteCPM() {
        return eCPM;
    }

    public void seteCPM(Double eCPM) {
        this.eCPM = eCPM;
    }

    public Double getRevenue() {
        return revenue;
    }

    public void setRevenue(Double revenue) {
        this.revenue = revenue;
    }

    public Long getRequests() {
        return requests;
    }

    public void setRequests(Long requests) {
        this.requests = requests;
    }


    /**
     * Calculates CTR, CR, Fill rate and eCPM values.
     */
    public void calculateRates(){
        BigDecimal clicks = getClicks() == null ? BigDecimal.ZERO : BigDecimal.valueOf(getClicks());
        BigDecimal impressions = getImpressions() == null ? BigDecimal.ZERO : BigDecimal.valueOf(getImpressions());
        BigDecimal conversions = getConversions() == null ? BigDecimal.ZERO : BigDecimal.valueOf(getConversions());
        BigDecimal requests = getRequests() == null ? BigDecimal.ZERO : BigDecimal.valueOf(getRequests());
        BigDecimal revenue = getRevenue() == null ? BigDecimal.ZERO : BigDecimal.valueOf(getRevenue());

        setCtr(calculate(MathFunctions.ctr, clicks, impressions).doubleValue());
        setCr(calculate(MathFunctions.cr, conversions, impressions).doubleValue());
        setFillRate(calculate(MathFunctions.fillRate, impressions, requests).doubleValue());
        seteCPM(calculate(MathFunctions.eCPM, revenue, impressions).doubleValue());
    }


    /**
     * Accumulates clicks, impressions, conversions, requests and revenue values of this with other AnalyticsDTO.
     * @param other Instance of AnalyticsDTO
     */
    public void sumValues(AnalyticsDTO other){
        setClicks(calculate(MathFunctions.sum, this.getClicks(), other.getClicks()).longValue());
        setImpressions(calculate(MathFunctions.sum, this.getImpressions(), other.getImpressions()).longValue());
        setConversions(calculate(MathFunctions.sum, this.getConversions(), + other.getConversions()).longValue());
        setRequests(calculate(MathFunctions.sum, this.getRequests(), other.getRequests()).longValue());
        setRevenue(calculate(MathFunctions.sumDouble, this.getRevenue(), other.getRevenue()).doubleValue());
    }
}
