package com.lfrefatti.adsreporting.dto;

import com.lfrefatti.adsreporting.swagger.DocumentedApiErrorDTO;
import org.springframework.http.HttpStatus;

public class ApiErrorDTO implements DocumentedApiErrorDTO{

    private HttpStatus status;
    private String message;

    public ApiErrorDTO(HttpStatus status, String message) {
        this.status = status;
        this.message = message;
    }

    public HttpStatus getStatus() {
        return status;
    }

    public String getMessage() {
        return message;
    }

}
