package com.lfrefatti.adsreporting.exception;

public class NotFoundException extends Exception {

    public NotFoundException(){
        super("No advertising data found for the given params.");
    }

}
