package com.lfrefatti.adsreporting.service;

import com.lfrefatti.adsreporting.dto.AnalyticsDTO;
import com.lfrefatti.adsreporting.utils.CsvReader;
import org.springframework.stereotype.Service;

import java.io.InputStream;
import java.util.List;
import java.util.function.BiFunction;

@Service
public class CsvParser {

    /**
     * Parses the csv file, applying the mapToAnalytics function
     * to map each line to a AnalyticsDTO object.
     *
     * @param is csv file input stream
     * @param separator csv values separator
     * @return
     */
    public List parse(InputStream is, String separator){
        return CsvReader.readCsvLines(mapToAnalytics, is, separator);
    }

    /**
     * Mapper function that produces AnalyticsDTO objects from a String
     */
    private BiFunction<String, String, AnalyticsDTO> mapToAnalytics = (line, separator) -> {
        String columns [] = line.split(separator);
        return new AnalyticsDTO(
                columns[0].replace(" ", "_"),
                Long.valueOf(columns[1].trim()),
                Long.valueOf(columns[2].trim()),
                Long.valueOf(columns[3].trim()),
                Long.valueOf(columns[4].trim()),
                Double.valueOf(columns[5].trim())
        );
    };

}
