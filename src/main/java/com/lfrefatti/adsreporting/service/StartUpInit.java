package com.lfrefatti.adsreporting.service;

import com.lfrefatti.adsreporting.dao.AnalyticsRepository;
import com.lfrefatti.adsreporting.dto.AnalyticsDTO;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import javax.annotation.PostConstruct;
import java.io.*;
import java.nio.file.Files;
import java.util.ArrayList;
import java.util.List;

@Component
public class StartUpInit {

    @Autowired
    CsvParser parser;

    @Autowired
    AnalyticsRepository dao;


    /** Load csv files from resources, parse the files
     * and saves the AnalyticsDTO to DB.
     * @throws IOException
     */
    @PostConstruct
    public void init() throws IOException {
        File[] csvs = getFiles();
        List<AnalyticsDTO> analytics = parseCsvs(csvs);
        dao.saveAll(analytics);
    }

    private List<AnalyticsDTO> parseCsvs(File [] files) throws FileNotFoundException {
        List<AnalyticsDTO> analytics = new ArrayList<AnalyticsDTO>();
        for (File file : files){
            String[] name = file.getName().split("_");
            if (!isValidCsvFile(name))
                continue;

            List<AnalyticsDTO> monthAnalytics = parser.parse(new FileInputStream(file), ",");

            monthAnalytics.stream().forEach(analytic -> {
                analytic.setMonth(Integer.valueOf(file.getName().split("_")[1]));
                analytic.calculateRates();
            });

            analytics.addAll(monthAnalytics);
        }
        return analytics;
    }

    private File[] getFiles() throws IOException {
        String pathToCsvFolder = getClass().getClassLoader().getResource("csv/").getPath();
        return new File(pathToCsvFolder).listFiles();
    }

    private boolean isValidCsvFile(String[] name){
        return name[name.length-1].endsWith(".csv") && name.length >=2 && Character.isDigit(name[1].charAt(0));
    }

}
