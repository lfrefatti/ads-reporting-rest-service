package com.lfrefatti.adsreporting.swagger;

import com.lfrefatti.adsreporting.dto.AnalyticsDTO;
import com.lfrefatti.adsreporting.dto.ApiErrorDTO;
import com.lfrefatti.adsreporting.exception.NotFoundException;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import org.springframework.web.bind.annotation.RequestParam;

import java.util.Optional;

@Api(value = "analytics", description = "", tags = "Advertising report REST API")
public interface DocumentedAdsReportController {

    @ApiOperation(value = "Returns the advertising data from one month and site, or from all sites, all months or all months and sites.", response = AnalyticsDTO.class)
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "Success result from requested data.", response = AnalyticsDTO.class),
            @ApiResponse(code = 400, message = "Bad Request", response = ApiErrorDTO.class),
            @ApiResponse(code = 404, message = "Data not found", response = ApiErrorDTO.class),
            @ApiResponse(code = 500, message = "Internal server error", response = ApiErrorDTO.class)})
    AnalyticsDTO getReport(@RequestParam("month") Optional<String> month,
                           @RequestParam("site") Optional<String> site)
                            throws NotFoundException;
}
