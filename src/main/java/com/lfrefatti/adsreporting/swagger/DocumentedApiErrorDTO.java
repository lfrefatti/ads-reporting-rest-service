package com.lfrefatti.adsreporting.swagger;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import org.springframework.http.HttpStatus;

@ApiModel(description = "Object that encapsulates API Errors attributes")
public interface DocumentedApiErrorDTO {

    @ApiModelProperty(notes = "The HTTP status code")
    HttpStatus getStatus();

    @ApiModelProperty(notes = "Error message to be reported to the consumer")
    String getMessage();
}
