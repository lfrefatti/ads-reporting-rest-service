package com.lfrefatti.adsreporting.swagger;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

@ApiModel(value = "Analytics", description = "Advertising data")
public interface DocumentedAnalyticsDTO {

    @ApiModelProperty(notes = "Month of the year where data was generated")
    String getMonthName();

    @ApiModelProperty(notes = "Site where the data were collected")
    String getSite();

    @ApiModelProperty(notes = "Number of impressions from the given site and month")
    Long getImpressions();

    @ApiModelProperty(notes = "Number of clicks from the given site and month")
    Long getClicks();

    @ApiModelProperty(notes = "Number of conversions from the given site and month")
    Long getConversions();

    @ApiModelProperty(notes = "Click-through rate from the given site and month")
    Double getCtr();

    @ApiModelProperty(notes = "Conversion rate from the given site and month")
    Double getCr();

    @ApiModelProperty(notes = "Ratio of impressions to the number of requests from the given site and month")
    Double getFillRate();

    @ApiModelProperty(notes = "Effective Cost Per Thousand from the given site and month")
    Double geteCPM();

    @ApiModelProperty(notes = "Total revenue amount from the given site and month")
    Double getRevenue();

    @ApiModelProperty(notes = "Number of requests from the given site and month")
    Long getRequests();

}
