package com.lfrefatti.adsreporting.dao;

import com.lfrefatti.adsreporting.dto.AnalyticsDTO;
import org.springframework.data.jpa.repository.JpaRepository;

public interface AnalyticsRepository extends JpaRepository<AnalyticsDTO, Integer> {

}