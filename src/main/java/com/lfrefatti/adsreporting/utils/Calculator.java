package com.lfrefatti.adsreporting.utils;

import java.math.BigDecimal;
import java.util.function.BiFunction;

public final class Calculator {


    /**
     * Applies the function f to the bigdecimal values value1 and value2
     * @param f
     * @param value1
     * @param value2
     * @return
     */
    public static BigDecimal calculate(BiFunction<BigDecimal, BigDecimal, BigDecimal> f, BigDecimal value1, BigDecimal value2){
        return f.apply(value1, value2);
    }

    /**
     * Applies the function f to the long values value1 and value2
     * @param f
     * @param value1
     * @param value2
     * @return
     */
    public static BigDecimal calculate(BiFunction<Long, Long, BigDecimal> f, Long value1, Long value2){
        return f.apply(value1, value2);
    }


    /**
     * Applies the function f to the double values value1 and value2
     * @param f
     * @param value1
     * @param value2
     * @return
     */
    public static BigDecimal calculate(BiFunction<Double, Double, BigDecimal> f, Double value1, Double value2){
        return f.apply(value1, value2);
    }

}
