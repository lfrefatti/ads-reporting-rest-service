package com.lfrefatti.adsreporting.utils;

import java.math.BigDecimal;
import java.util.function.BiFunction;

public class MathFunctions {

    private static final Integer SCALE_TWO = 2;
    private static final BigDecimal ONE_HUNDRED = BigDecimal.valueOf(100);
    private static final BigDecimal ONE_THOUSAND = BigDecimal.valueOf(1000);

    /**
     * Calculates click-through rate, the ratio of users who click on a specific
     * link to the number of total users who view an advertisement.
     * obs: result precision scale is set to two.
     */
    public static BiFunction<BigDecimal, BigDecimal, BigDecimal> ctr = (clicks, impressions) ->
            clicks.divide(impressions, SCALE_TWO, BigDecimal.ROUND_HALF_UP).multiply(ONE_HUNDRED);

    /**
     * Calculates conversion rate. The ratio of conversions to the number of impressions.
     * obs: result precision scale is set to two.
     */
    public static BiFunction<BigDecimal, BigDecimal, BigDecimal> cr = (conversions, impressions) ->
        conversions.divide(impressions, SCALE_TWO, BigDecimal.ROUND_HALF_UP).multiply(ONE_HUNDRED);

    /**
     * Calculates the ratio of impressions to the number of requests.
     * obs: result precision scale is set to two.
     */
    public static BiFunction<BigDecimal, BigDecimal, BigDecimal> fillRate = (impressions, requests) ->
        impressions.divide(requests, SCALE_TWO, BigDecimal.ROUND_HALF_UP).multiply(ONE_HUNDRED);

    /**
     * Calculates Effective Cost Per Thousand.
     * A translation from CPM, expressed as such from a publisher's point of view.
     * obs: result precision scale is set to two.
     */
    public static BiFunction<BigDecimal, BigDecimal, BigDecimal> eCPM = (revenue, impressions) ->
        revenue.multiply(ONE_THOUSAND).divide(impressions, SCALE_TWO, BigDecimal.ROUND_HALF_UP);


    /**
     * Calculates the sum of two long values, if any of the values is null, then it will be set to zero.
     */
    public static BiFunction<Long, Long, BigDecimal> sum = (value1, value2) -> {
        BigDecimal bdValue1 = value1 == null ? BigDecimal.ZERO : BigDecimal.valueOf(value1);
        BigDecimal bdValue2 = value2 == null ? BigDecimal.ZERO : BigDecimal.valueOf(value2);
        return bdValue1.add(bdValue2);
    };

    /**
     * Calculates the sum of two double values, if any of the values is null, then it will be set to zero.
     */
    public static BiFunction<Double, Double, BigDecimal> sumDouble = (value1, value2) -> {
        BigDecimal bdValue1 = value1 == null ? BigDecimal.ZERO : BigDecimal.valueOf(value1);
        BigDecimal bdValue2 = value2 == null ? BigDecimal.ZERO : BigDecimal.valueOf(value2);
        return bdValue1.add(bdValue2);
    };
}
