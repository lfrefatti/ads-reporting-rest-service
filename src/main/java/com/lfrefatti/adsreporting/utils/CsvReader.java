package com.lfrefatti.adsreporting.utils;

import com.lfrefatti.adsreporting.dto.AnalyticsDTO;

import java.io.BufferedReader;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.List;
import java.util.function.BiFunction;
import java.util.stream.Collectors;

public class CsvReader {

    /**
     * Reads the 1st line from the input stream and returns a list with columns names
     * @param is
     * @param separator
     * @return
     */
    public static String[] readCsvColumns(InputStream is, String separator){
        BufferedReader reader = getBufferedReader(is);
        return reader.lines().findFirst().map(line -> line.split(separator)).get();
    }

    /**
     * Reads the lines from the csv file and applies the mapper function
     * producing a list of objects.
     * @param mapper
     * @param is
     * @param separator
     * @return
     */
    public static List<? extends CsvParserContract> readCsvLines(BiFunction<String, String, ? extends CsvParserContract> mapper, InputStream is, String separator){
        BufferedReader reader = getBufferedReader(is);
        return reader
                .lines()
                .skip(1)
                .map(line -> mapper.apply(line, separator))
                .collect(Collectors.toList());
    }

    private static BufferedReader getBufferedReader(InputStream is) {
        return new BufferedReader(new InputStreamReader(is));
    }

}
