package com.lfrefatti.adsreporting.controller;

import com.lfrefatti.adsreporting.dto.ApiErrorDTO;
import com.lfrefatti.adsreporting.exception.NotFoundException;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.ResponseStatus;

import javax.servlet.http.HttpServletRequest;
import java.time.DateTimeException;

@ControllerAdvice
public class ApiExceptionHandler {

    private final Logger logger = LogManager.getLogger(getClass());

    @ExceptionHandler({NotFoundException.class})
    @ResponseStatus(HttpStatus.NOT_FOUND)
    public @ResponseBody ApiErrorDTO handleNotFoundException(Exception e){
        return handleException(e, HttpStatus.NOT_FOUND);
    }

    @ExceptionHandler({DateTimeException.class, IllegalArgumentException.class})
    @ResponseStatus(HttpStatus.BAD_REQUEST)
    public @ResponseBody ApiErrorDTO handleBadRequestException(Exception e){
        return handleException(e, HttpStatus.BAD_REQUEST);
    }

    @ExceptionHandler({Exception.class})
    @ResponseStatus(HttpStatus.INTERNAL_SERVER_ERROR)
    public @ResponseBody
    ApiErrorDTO handleGenericException(Exception e){
        return handleException(e, HttpStatus.INTERNAL_SERVER_ERROR);
    }

    private ApiErrorDTO handleException(Exception e, HttpStatus status) {
        logger.error(e);

        String message = e.getMessage();

        if(message == null)
            message = e.toString();

        return new ApiErrorDTO(status, message);
    }

}
