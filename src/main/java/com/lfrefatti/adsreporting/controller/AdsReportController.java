package com.lfrefatti.adsreporting.controller;

import static org.springframework.http.MediaType.*;
import static org.springframework.web.bind.annotation.RequestMethod.*;

import com.lfrefatti.adsreporting.dao.AnalyticsRepository;
import com.lfrefatti.adsreporting.dto.AnalyticsDTO;
import com.lfrefatti.adsreporting.enums.Months;
import com.lfrefatti.adsreporting.exception.NotFoundException;
import com.lfrefatti.adsreporting.swagger.DocumentedAdsReportController;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Example;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;

import java.time.Month;
import java.util.List;
import java.util.Optional;

@RestController
public class AdsReportController implements DocumentedAdsReportController{

    @Autowired
    AnalyticsRepository dao;

    @RequestMapping(value = "/reports", method = {GET}, produces = APPLICATION_JSON_UTF8_VALUE)
    @ResponseStatus(HttpStatus.OK)
    public @ResponseBody AnalyticsDTO getReport(@RequestParam("month") Optional<String> month,
                                                @RequestParam("site") Optional<String> site)
                                                throws NotFoundException {
        return findAnalytics(month, site);
    }

    private AnalyticsDTO findAnalytics(Optional<String> month, Optional<String> site) throws NotFoundException {
        AnalyticsDTO result = new AnalyticsDTO();

        if (month.isPresent()){
            Integer monthNumber = validateMonth(month.get());
            result.setMonth(monthNumber);
        }

        if (site.isPresent())
            result.setSite(site.get());

        List<AnalyticsDTO> results = dao.findAll(Example.of(result));

        if(results.isEmpty())
            throw new NotFoundException();

        results.stream().forEach(analytic -> result.sumValues(analytic));
        result.calculateRates();

        return result;
    }

    private Integer validateMonth(String month) {
        if(Character.isDigit(month.charAt(0)))
            return validateMonthNumber(month);

        if(month.length() == 3)
            return Months.valueOf(month.toUpperCase()).getValue();

        return Month.valueOf(month.toUpperCase()).getValue();
    }


    private Integer validateMonthNumber(String month) {
        Integer monthNumber = Integer.valueOf(month);
        Month.of(monthNumber);
        return monthNumber;
    }

}
