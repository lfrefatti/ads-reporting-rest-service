package com.lfrefatti.adsreporting.integration;

import static org.hamcrest.Matchers.*;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.context.web.WebAppConfiguration;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.web.context.WebApplicationContext;

@RunWith(SpringRunner.class)
@SpringBootTest
@WebAppConfiguration
public class AdsReportingIT {

	private MockMvc mockMvc;

	@Autowired
	private WebApplicationContext webApplicationContext;

	@Before public void
	setUp(){
		mockMvc = MockMvcBuilders.webAppContextSetup(webApplicationContext).build();
	}

	@Test public void
	should_receive_response_with_status_code_200_when_request_report_without_params() throws Exception {
		mockMvc.perform(get("/reports"))
				.andExpect(status().isOk())
				.andExpect(jsonPath("$.clicks", is(Integer.valueOf(184724))))
				.andExpect(jsonPath("$.requests", is(Integer.valueOf(68823820))))
				.andExpect(jsonPath("$.impressions", is(Integer.valueOf(64422713))))
				.andExpect(jsonPath("$.conversions", is(Integer.valueOf(39477))))
				.andExpect(jsonPath("$.revenue", is(Double.valueOf(128351.91))))
				.andExpect(jsonPath("$.CTR", is(Double.valueOf(0))))
				.andExpect(jsonPath("$.CR", is(Double.valueOf(0))))
				.andExpect(jsonPath("$.fill_rate", is(Double.valueOf(94))))
				.andExpect(jsonPath("$.eCPM", is(Double.valueOf(1.99))));
	}

    @Test public void
    should_receive_response_with_status_code_200_and_have_month_property_when_request_report_with_month_param() throws Exception {
        mockMvc.perform(get("/reports?month=1"))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.month", is("January")));
    }

    @Test public void
    should_receive_response_with_status_code_200_and_have_month_and_site_properties_when_request_report_with_month_and_site_params() throws Exception {
        mockMvc.perform(get("/reports?month=1&site=iOS"))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.month", is("January")))
                .andExpect(jsonPath("$.site", is("iOS")));
    }

    @Test public void
    should_receive_response_with_status_code_200_and_have_site_properties_when_request_report_with_site_param() throws Exception {
        mockMvc.perform(get("/reports?site=iOS"))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.site", is("iOS")));
    }

    @Test public void
    should_receive_response_with_status_code_404_and_message_data_not_found_when_month_is_March() throws Exception {
        mockMvc.perform(get("/reports?month=3"))
                .andExpect(status().isNotFound())
                .andExpect(jsonPath("$.message", is("No advertising data found for the given params.")));
    }

    @Test public void
    should_receive_response_with_status_code_400_and_message_bad_request_form_month_13() throws Exception {
        mockMvc.perform(get("/reports?month=13"))
                .andExpect(status().isBadRequest())
                .andExpect(jsonPath("$.message", is("Invalid value for MonthOfYear: 13")));
    }

    @Test public void
    should_receive_response_with_status_code_400_and_message_bad_request_form_month_JEN() throws Exception {
        mockMvc.perform(get("/reports?month=JEN"))
                .andExpect(status().isBadRequest())
                .andExpect(jsonPath("$.message", is("No enum constant com.lfrefatti.adsreporting.enums.Months.JEN")));
    }

}
