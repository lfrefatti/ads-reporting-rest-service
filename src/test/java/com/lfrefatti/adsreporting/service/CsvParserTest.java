package com.lfrefatti.adsreporting.service;

import com.lfrefatti.adsreporting.dto.AnalyticsDTO;
import org.junit.Before;
import org.junit.Test;

import java.io.IOException;
import java.io.InputStream;
import java.util.List;

import static org.junit.Assert.*;

public class CsvParserTest {

    private CsvParser reader;

    @Before
    public void setUp(){
        reader = new CsvParser();
    }

    @Test public void
    should_return_a_list_of_Analytics_with_4_elements_when_csv_file_is_parsed() throws IOException {
        InputStream is = getClass().getClassLoader().getResourceAsStream("csv/2018_01_report.csv");
        List<AnalyticsDTO> analytics = reader.parse(is, ",");
        assertNotNull(analytics);
        assertEquals("List size", 4, analytics.size());
    }

}