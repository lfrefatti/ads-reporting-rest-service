package com.lfrefatti.adsreporting.utils;

import org.junit.Test;

import java.math.BigDecimal;

import static com.lfrefatti.adsreporting.utils.Calculator.*;
import static com.lfrefatti.adsreporting.utils.MathFunctions.*;
import static org.junit.Assert.*;


public class CalculatorTest {

    @Test public void
    should_return_ctr_value_when_clicks_is_30965_and_impressions_is_11866157(){
        BigDecimal clicks = BigDecimal.valueOf(30965);
        BigDecimal impressions = BigDecimal.valueOf(11866157l);
        assertEquals("Wrong CTR", BigDecimal.ZERO.setScale(2), calculate(ctr, clicks, impressions));
    }

    @Test public void
    should_return_cr_value_when_conversions_is_7608_and_impressions_is_11866157(){
        BigDecimal conversions = BigDecimal.valueOf(7608);
        BigDecimal impressions = BigDecimal.valueOf(11866157l);
        assertEquals("Wrong CR", BigDecimal.ZERO.setScale(2), calculate(cr, conversions, impressions));
    }

    @Test public void
    should_return_fill_rate_value_when_impressions_is_11866157_and_requests_is_12483775(){
        BigDecimal impressions = BigDecimal.valueOf(11866157l);
        BigDecimal requests = BigDecimal.valueOf(12483775);
        assertEquals("Wrong fill rate", BigDecimal.valueOf(95).setScale(2), calculate(fillRate, impressions, requests));
    }

    @Test public void
    should_return_eCPM_value_when_revenue_is_23555_46_and_impressions_is_11866157(){
        BigDecimal revenue = BigDecimal.valueOf(23555.46d);
        BigDecimal impressions = BigDecimal.valueOf(11866157l);
        assertEquals("Wrong eCPM", BigDecimal.valueOf(1.99).setScale(2), calculate(eCPM, revenue, impressions));
    }

}