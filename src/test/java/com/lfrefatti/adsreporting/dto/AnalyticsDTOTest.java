package com.lfrefatti.adsreporting.dto;

import org.junit.Test;

import static org.junit.Assert.*;

public class AnalyticsDTOTest {

    @Test public void
    should_calculate_values_for_ctr_cr_fillRate_eCPM_when_call_method_calculateRates(){
        AnalyticsDTO analytics = new AnalyticsDTO("mobile web", 9905942l, 9401153l, 25291l, 6216l, 19053.61d);
        assertNull(analytics.getCr());
        assertNull(analytics.getCtr());
        assertNull(analytics.getFillRate());
        assertNull(analytics.geteCPM());
        analytics.calculateRates();
        assertEquals("CR", Double.valueOf(0), analytics.getCr());
        assertEquals("CTR", Double.valueOf(0), analytics.getCtr());
        assertEquals("Fill rate", Double.valueOf(95), analytics.getFillRate());
        assertEquals("eCPM", Double.valueOf(2.03d), analytics.geteCPM());
    }

    @Test public void
    should_sum_values_from_analytics_with_other_analytics(){
        AnalyticsDTO analytics = new AnalyticsDTO("mobile web", 9905942l, 9401153l, 25291l, 6216l, 19053.61d);
        AnalyticsDTO other = new AnalyticsDTO("mobile web", 9905942l, 9401153l, 25291l, 6216l, 19053.61d);
        analytics.sumValues(other);
        assertEquals("clicks", Long.valueOf(50582l), analytics.getClicks());
        assertEquals("impressions", Long.valueOf(18802306l), analytics.getImpressions());
        assertEquals("conversions", Long.valueOf(12432l), analytics.getConversions());
        assertEquals("requests", Long.valueOf(19811884l), analytics.getRequests());
        assertEquals("revenue", Double.valueOf(38107.22d), analytics.getRevenue());
    }

}